title: An Open Letter to the FCC and Other Net-Neutrality Opponents
author: jcboysha
description: 
post_id: 288
created: 2014/06/03 19:46:39
created_gmt: 2014/06/03 23:46:39
comment_status: open
post_name: an-open-letter-to-the-fcc-and-other-net-neutrality-opponents
status: publish
post_type: post

# An Open Letter to the FCC and Other Net-Neutrality Opponents

As technology companies continue to implement traffic throttling, data caps, and other measures by hiding behind the guise of bad excuses, lies, and outright pointing to profit margins that aren't big enough the consumers of the world are suffering more and more. Many providers are researching data caps that would cause eCommerce via the sale of digital goods to all but disappear (If you have a 17 gigabyte transfer cap, and you play an online game, you might as well just give up trying, much less if you want more than one movie a month). Digital stream providers (Netflix, Hulu, Amazon, Etc) are going to be outright blocked from the market by cable providers if the trend continues. Cable companies ARE NOT providing an environment conducive to the very ideals that the internet was conceived upon.

In this technology infused time it is important to realize the implications that could come from such dangerous things as allowing internet access to be controlled by a corporate monopoly, and preventing fair and equal access to all services provided thereon. This is damaging to the spread of information, and puts the control of possible censorship in the hands of a private industry that has, time and time again, proven it cannot be trusted with such a large responsibility.

The internet, in its idealized form, would treat actions such as censorship, bandwidth limitation, traffic prioritization as damage. The natural course of a network such as this would be to redirect traffic around the offending authority (or node). However, over the development of the internet it has become a non-ideal network, with single choke points, and control nodes. This star-tree topology was, I do believe, not put in place by accident. I believe this was put in place by companies, governments, and other network authorities who want to ensure their complete control over the flow of network traffic.

The broadband internet and cable industry needs to come to the realization that their actions will bear important consequences for the future. These consequences control the future of intelligence, privacy, the continuing education of the American people, and the fate of most other important industries in the country, and the world.

It is important to note that the internet is the most unifying concept the world has ever known. I can equally interact with colleagues and friends 5 miles away as I can on the opposite side of the world. Hindering this unequivocal potential for communication and unity across the globe would be damaging to say the least, catastrophic at worst.

By allowing these companies to continue unhindered, and in some appalling ways even supported, the internet will come crumbling down around you. You will have only yourselves to blame. Resolve this issue now by reclassifying broadband providers as public utilities and fostering competition in a sparse market. Help the internet heal and become better, don’t deliver its final blow. 

