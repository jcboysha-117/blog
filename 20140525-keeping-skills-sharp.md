title: Keeping Skills Sharp
author: jcboysha
description: 
post_id: 248
created: 2014/05/25 10:00:14
created_gmt: 2014/05/25 14:00:14
comment_status: open
post_name: keeping-skills-sharp
status: publish
post_type: post

# Keeping Skills Sharp

Over the years there are things that we all learn how to do. Perhaps it was once a hobby, or just a fleeting interest. Maybe it was something you really loved but couldn't keep up with, or life got in the way. Regardless we all have those skills we just let fall by the wayside for one reason or another. It's a sad thing to think about when that happens. We look back on times we could do something and wish we still could. We remember all of the effort we put into learning a skill. Most often when this happens we try to jump back into it like we never left. We jump right into the cold dark deep end of the pool and find ourselves struggling to stay afloat. This, especially if your skill is swimming, is a good way to never accomplish your skill again. You'll get discouraged and feel downtrodden. You'll associate discomfort and disappointment with your skill. Try starting over. Re-learning a skill is much easier the second time around. You'll find yourself doing practice exercises you know and getting better at them than you were previously. You'll relearn some nuance that you skipped the first time. You'll, most likely, even improve your skill so that when you get proficient again, you're better than you were. It's an amazing sensation to re-accomplish something. More-so, I would say, than it is to accomplish it the first time. When you re-accomplish a task like this you can remind yourself that you are as sharp as you once were, and now you can continue to get even sharper. Whatever your skill is, whatever you happen to want to accomplish now, go do it! Go relearn a skill and get better than you were the last time you did it. You'll be proud of yourself, feel great, and have a renewed happiness in life.