# Managing Change

Change Management is probably one of the biggest things to consider when trying to develop a secure infrastructure. We've talked about CIA, and we've talked about how to (very generally) analyze risk and determine where you are weak, and how to correct those risks. However, I pose to you a question: _how do you determine risks, or protect data if you don't know what you have?_

This is where change management comes in. Before we really dive into this, however, let's take a minute to lay out some assumptions. 

1) You know what physical assets you have in your environment (Asset Management)

2) You know what data assets you have in your environment (Information Management) 

3) You know what regulations and guidelines you have to follow for legal or organizational reasons (Governance, Regulation, and Compliance) 

Assuming all of these things have already been lain out for you, you have to go through and start worrying about where your environment might change from one week to the next, from one day to the next, or even from one hour to the next. Any change in the physical makeup of your environment can create new vectors both from a physical and a logical security perspective (New machines can have new hardware that is untested, new software that is untested, or either that are tested but have new Zero-Day vulnerabilities). These changes are important to consider, ideally before they happen, but definitely in retrospect so the Operations or Security group at your organization can learn what went well or poorly in implementing the change. 

## How do I manage change?

This is probably the best question I've ever heard asked. Mainly, because it is impossible to answer. Every environment manages change differently. You have ITIL standards, you've got ISO suggestions (in any number of standards), you've got other proprietary recommendations, and even further random governing bodies and organizations will have different suggestions. At the core, however, we can start to explore the answer to this question. Every change management practice has at least a few things in common. We can use these to go over some general rules. 

There will generally be between 6 and 7 steps from planning to review of implementation that you will need to track to have a reasonable change management plan. 

### Plan the Change

First, the engineer behind the change is going to need to write up a plan to make a change to the environment. These plans can be anything as simple as a couple lines in a document saying the engineer plans to update a piece of software in production to as complex as a rollout of a new make of laptop to the end-users of the organization. 

This plan will help decision makers assess the impact of the change and determine whether or not to act. 

### Request and Review the Change

Once the engineer has their plan written out, they submit it for implementation. In this case, decision-makers, managers, directors, or whoever else reviews the change for potential impact to the environment as a whole. This includes performing potential business impact analyses, potential security impacts, or potential risks of the change. Assuming all goes well, the decision-maker will then approve the change for implementation. 

### Prepare the Change

At this point, the engineer now knows that their change is approved for implementation. That does not mean that our intrepid engineer can just drop changes into production. Here, the engineer will begin preparing whatever specifics they need. Tools that need to be installed, or scripts that will be needed for the implementation, are generated or prepared in this step. Additionally, a conceptual run of the change is made by the implementing engineer to ensure they have everything they need to implement the change. 

### Test the Change

Our engineer will now take their prepared tools and implement the change in a test setting. This could be a purpose-built test environment of VMs, a trial run of the new machines, or a full reproduction of the production environment for testing purposes. The engineer will go through the steps in the change plan and ensure that there are no unexpected consequences, that the change took properly, and that there is no resultant instability or blatantly obvious attack vectors. 

### Review the Test
After the change has been made in testing, the engineer will both do a personal review, and have another engineer perform a peer review to ensure that the change was successful in the testing environment. 

### Implement the Change
Once the steps have been tested, and it is certain that they work, the change can be implemented in production. Following the steps given in the implementation plan the change can be implemented in the actual production enviornment. At this point, adrenaline should be flowing because everything could still go wrong. 

### Review the Implementation
Assuming nothing goes wrong, and the implementation is not hindered in any way, now is the time to make sure it all works. The engineer should perform a review and a peer should review the implementation to ensure that everything went according to plan. Whether it does or not, now is the time to look back and learn what you can. Write down all of the expected things, and unexpected things, that went both right and wrong with the implementation. These notes will be truly beneficial later on when trying to make similar, or in some cases, extremely different but relevant other changes to the environment. 

## Overall
More or less, those are the only steps to making a change in an environment in a well managed fashion. Different methodologies will use different terms, or have slightly different or re-ordered steps. Either way, this is extremely important to ensuring that any changes in your environment and their potential impact are well documented to be reviewed for security of the environment or system the next time a secure review comes up. 

