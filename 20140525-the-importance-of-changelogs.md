title: The Importance of Changelogs
author: jcboysha
description: 
post_id: 257
created: 2014/05/25 10:06:31
created_gmt: 2014/05/25 14:06:31
comment_status: open
post_name: the-importance-of-changelogs
status: publish
post_type: post

# The Importance of Changelogs

Lately I've been working on getting some OS images together for my workplace. As I've been doing this I'll, occasionally, find myself with a mistake that makes me backtrack and start from a different image. I often find myself repeating work. It kills me to do that, especially because I often do something and forget I did it. So it leads to me repeating more work. It ends up taking me twice as long to get a task done and I feel poorly afterwards. I could save myself a whole bunch of trouble if I kept better Changelogs. Changelogs are just what they sound like. It's a series of notes that keep track of what changed between revisions of an object. This object can be a Document like a chart, or a spreadsheet, or a presentation, or it could be a piece of code for software or an OS image. All of these objects can benefit from keeping meticulous notes on what is being accomplished and when. This can not only help you if you have to go back to re-do something, but it can help anyone else who might later join the project. In the professional world, having these changelogs can make you seem even more like a Guru because not only do you know what you did, but you know WHEN you did it. You can give dates, times, revisions, and any other information that might be needed when you hand a project over. Remember to keep good changelogs. It'll help you in your projects, it'll make you look good, and it'll help anyone who might join you later on.