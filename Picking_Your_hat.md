# Picking your Hat

Or how to get started hacking...

## Summary

CyberSecurity is a growing field. This, coupled with the love of hacking that people seem to get more and more in this world of constant learning, has prompted me to offer up this series of steps and things to learn if you want to get started. This is mostly going to be a post with a repository of resources than specific instructions. 

### I will not teach you how to hack 

There are hundreds of resources out there for how to hack. They'll teach you specific steps to exploit specific things and use specific tools. This will make you a script kiddie, and it will be very hard to progress from there. My goal is to teach you how to learn, and how to get better.

***This post will be regularly updated with more resources and new updates. Keep coming back for more.***

## First Steps, What should I learn?

First, there are some core things that every hacker needs to know. You've got to have some basic systems knowledge, some basic network knowledge, and some basic programming knowledge. 

### How to Learn

With all of the resources I give you, you should approach them with the following ideaology: you need to do some basic reading on what these things are (be sure to remember the key points), and understand what you're doing. This can be done by applying the knowledge with some controlled tasks. Once you get through there you can analyze and evaluate new information, and finally you can create new ideas, and take existing tools and use them in new ways. 

### Resources
For Coding: 
* Codeacademy.com
 * This is probably the best resource for messing around with a programming language. It's an automated series of tasks that walk you through learning. I suggest their Python course for hacking. 

For Systems: 
* https://www.edx.org/course/introduction-to-linux
 * Free EdX course on Linux (Endorsed by Linus Torvalds himself)
* https://courses.linuxtrainingacademy.com/itsfoss-ll5d/
 * Learn Linux in 5 Days is a free book from the Linux Foundation that will help you get started with the basics

For Networks: 
* This one is a little bit harder and I'm going to give a list of topics to do some research on: 
 * the OSI Model
 * TCP/IP
 * TCP vs UDP
 * Symmetric and Asymmetric Cryptography (Specifically SSL, SSH, Diffie Helman, and RSA) 
  * For the love of all things: Don't try to understand the math. It's going to be damn near impossible. 

General Things: 
* 2600: The Hacker Quarterly
 * A paper-published hacking journal with a lot of articles about philosophy and technical aspects of hacking
* Phrack
 * Kind of like 2600, but more awesome super technical and much more underground. Raw hacking info at its finest
* Subreddits
 * r/netsec
 * r/asknetsec
 * r/hacking
 * r/netsecstudents
 * There are a ton more. Go find subreddits, ask questions, start interacting. It's awesome. 
* Books
 * Never turn down a book on technology. No matter how old. Look for all the tech books you can find online, in book stores, at thrift stores. You can always learn something new. Even if you just get a new perspective on knowledge you already know.

Games: 

There are a lot of games that let you learn hacking concepts without the potential of breaking things (even insofar as being able to hack other players). These are my favorites: 
* Orwell
 * Good game for Social Engineering, Investigative Reasoning, and Data Acquisition
* Grey Hack
 * Good game for general hacking skills, can be played single or multiplayer
* HackMud
 * Good game for terminal skills, general hacking skills, and learning to fight intelligent adversaries: Multiplayer Only
* Uplink: Hacker Elite
 * OLD GAME, good for general hacking mindset
* Papers Please
 * A bit like Orwell, good for investigative reasoning
* TIS-100
 * Good for Alogrithm design and Assembly learning
* Shenzhen I/O
 * Good for learning the whole process of product design and a tinkering mindset
* Exapunks
 * Kinda like Shenzhen I/O but for software development instead of engineering
* Screeps
 * Programming AI for playing a minimally interactive RTS 

## Second Steps, What should I do?
Once you've got a good background, you've played around with a VM, and you can write a simple program, it's time to get hacking. 

***Most important: DO NOT TRY TO HACK THINGS THAT ARE NOT YOURS OR ARE NOT SPECIFICALLY DESIGNED FOR IT*** You will go to jail, no one will be happy. 

Go create an account at Wechall.org so you can track your progress. It'll give you cred and you can see how good you're getting with your scores increasing. 

Resources: 
* Hackthissite.org
 * This was my first hacking training site. It has a series of progressively harder challenges for web-based hacking, crypto, stegonography, and some app-sec hacking. 
* OverTheWire Wargames
 * These are kind of like a persistent CTF that let you hack either real linux systems, web-based applications, cryptography, etc. 
* Vulhub
 * Vulnhub offers a bunch of pre-built vulnerable system images that you have to get specific data off of for training purposes. 

## Third Steps, What's Next?'
Once you're doing hacking challenges left and right, you're getting better, you're talking to people on reddit, on forums, on facebook, you're already there. There is no next. You're a hacker. Now keep learning, keep growing, and above all keep hacking!
