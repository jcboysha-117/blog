# What *Real* Sysadmins Do

## What drives sysadmins?
Last month's Linux Admin Magazine had a line in the opening letter from the editor that talked about Junior Sysadmins wanting to do what the "Real" Sysadmins did. I think it is misleading to think "Real" Sysadmins do anything different from the juniors really (at least the "Real" Junior Sysadmins). 

Systems Administration, or Systems Engineering, Network Administration, Network Engineering, Security Analysis/Engineering, Desktop Support, Software Development, and really every Information Technology field have very similar driving forces. At the heart of all people who are really good at these jobs is a drive to learn more, to get better, and to make your ideas reality. Just like any good artist, InfoTech folks have big ideas; most of us have HUGE ideas. What separates the good from the frustrated is the ability to manifest those ideas into reality. It is, however, super important to note that I said frustrated and not bad. I do not think there are any people who have the drive, but can't do the work. There are, however, people who don't have the drive; this doesn't make them bad, but it does make them ill-suited for the InfoTech world. 

## Making the difference
When I participate in interviews, or when I give one-on-one interviews, I look for one thing: Your drive. (Depending on the position level, for sure) I don't care how much detail you can use to describe the OSI model, I don't care that you can hand write a Powershell Script and use OCR to make it run, and I really don't care how many Cisco commands you have memorized. Your resume would have told me how technical you are, and any certs/education would have built the rest of that expectation.<sup>1</sup> 

What I care about is how you think, not that you get the right answer. I want to see your process. I don't want to ask you to tell me what's wrong with a printer and have you spit out a text-book response: I want to hear you say "Well, I have no idea, but here's what I'd start with, and where I'd go from there". You don't even have to get to the solution to the problem! I just want to see that you have good iterative methods (and that you're willing to say you don't know). 

I care about your hobbies, and what draws you to them. I care that if I ask about things you enjoy doing that you get super passionate and nerd out a little bit. Whether it's a tech thing, cars, knitting, or basket-weaving I just want to see the fire-burning passion of someone who has a drive to learn. 

I've never met a person who couldn't learn the technical side of things. I've met a lot of people, however, who were super book smart but had absolutely NO idea how to solve a problem. The kind of person who could give me details about Lambda Calculus and explain the OSI model to a level that I'd have to spend a week reading up again just to understand what they said, but couldn't figure out that the cable came unplugged. 

## Fucking up, a lot. 
One of the biggest things I've learned after being in this world is how much everyone fucks up. I've never known ANYONE in this industry without at least 5 fun catastrophe inducing stories. The trick is we all own up to our mistakes. If you delete a production database, that is NOT the time to try and hide your problem and fix it yourself; it is the time to go to at least one other person and say "Yo, so I fucked up and we have to fix this..." It's a lesson you shouldn't have to learn, and if you do learn it you only learn it once: If you own your problems, it's ok to laugh and be laughed at, if you try to hide your problems, people will laugh at you after you've been fired. 

Never get confused and think that senior admins, developers, and engineers never fuck up. We're just so good at owning up to our mistakes, and making sure they get fixed that even the biggest deals don't seem like big deals. 

## Here for the fun, not for the pay. 
The last bit in this post/rant hybrid thing is one that comes up a lot. I see so many people in InfoTech for the money. They hate the job, they hate the hours, they hate the life, they want the pay. Fewer and fewer places are paying that BIG entry level salary anymore. Fewer and fewer places are paying anyone with seeming technical accumen huge salaries because they have a dire NEED for IT people. It's not about the money for us. We have jobs that we're passionate about. We love our work, we take pride in it, and we often love the company(ies) we work for. We kind of have to. If we didn't, we'd burn out the third time one busy month that we get called at 2 AM just to be told there was a breach and we have to spend the next 10 hours reading logs on almost no sleep. 

Pretty much all jobs in InfoTech are the kinds of jobs that if you don't love them, not only will the work be unfulfilling, but forcing yourself to go to work will get harder and harder until you break. These jobs never get easier; but if you love them, they get more fun. 

I've been a mercernary in this industry; hopping from one job to the next for money. It is damn miserable. Trust me: find a place you love, that you love the culture, that you love the mission, and that you love the job. Once you do, never let them go. Ever. Fight for them, make sure they don't change too much unless it is for the better. 

## Answer the question, dammit. 
So, that's the essence of it, but here it is summarized: Sysadmins love their jobs and learn constantly. That's what we do; that's what ALL (good) InfoTech people do. We have to keep learning to stay ahead of the problems that we face every day, improve our systems, and stay competitive in a field where if you take a two-week vacation you'll have to catch up on industry change. We love our jobs and our missions because if we didn't they'd eat us alive. So, if you're just starting out, and you look at the seniors and we look like we have everything under control, we never make mistakes, and we never break: know that we're just so far into this world that the insanity became comforting. We can even laugh about deleting a productiond database (nervously, while scrambling to fix it, but we laugh).


1. Yes, I DO verify you have the competency level you claim to have in interviews. But I'm making a point here: don't be pedantic. 
