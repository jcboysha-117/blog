title: IT as a Service
author: jcboysha
description: 
post_id: 289
created: 2014/12/22 21:15:34
created_gmt: 2014/12/23 02:15:34
comment_status: open
post_name: it-as-a-service
status: publish
post_type: post

# IT as a Service

It's been a while since my last post. After a crazy work season and some personal issues I find myself back in a place to begin blogging again. IT as a Service is popping up all over the place lately. The Software as a Service ideology (SaaS) has been around for a while, but now I've seen Networking as a Service, Project Management as a Service, and (perhaps most terrifying) Development as a Service. I think all of this needs to stop. Don't get me wrong here, customer service is an extremely important part of any business and has it's place. I would say any manager or customer relationship professional needs to have a wealth of customer service ability. However, as an IT pro, I wouldn't want anyone I'd want developing (or engineering a network, or even project managing) speaking with a Client (or Customer). Developers should think in terms that would make a client quite confused (object orientation and algorithmic design are outside of the average person's general thought processes; while developers need to think like that). Network engineers should be thinking about physical and data-link connections in their sleep and express their thoughts accordingly. That's not to say these groups don't have the command of the topics well enough to explain them to the average user, or anyone else for that matter, but they shouldn't be constantly switching trains of thought to these simpler terms and analogies on the day-to-day. Doing this makes their brains constantly switch-tasking and causing a loss of productivity. The industry, as a whole, would benefit from a deeper segregation between technical and client (or customer) facing employees. This segregation would allow for groups to focus on one task, at which each team member could specialize in, and improve their abilities in one arena. This puts all resources in a positive direction instead of one person attempting to live both worlds. What are your thoughts on the matter? Share 'em below! Let's start a conversation.

## Comments

**[Aaron McGrady](#5 "2014-12-24 04:00:34"):** Where i work, we develop software for a governmental client and we have only the leads of the project interact with the customer. Our team lead has many hours each sprint for client management and our tech lead backs him up with technical expertise in answering the whys and why nots of managing a clients expectation. I like this approach better than the customer facing app developer. I have worked in such a role and the stress of managing client exceptions along with developing a first rate project takes two different mind sets and slows productivity in each endeavor..

