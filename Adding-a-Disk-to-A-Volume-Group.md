# Adding a Disk to a Volume Group

## Summary
We've all been there. You're administrating a complicated server, the drive is running out of space, and you have to add more disk space to the system. Luckily, we thought this through and set it up with Volume Groups. All we have to do is add a drive; once this is done we have more space. 

No. 

You then have to go through a series of steps to make sure everything expands correctly, and the filesystem takes all the space it needs. Luckily, whether you're adding a physical drive to a physical machine or a virtual drive to a virtual machine, the steps are largely the same. We're going to assume you're on \*nix with a blank disk for this. 

## Steps

1) Add your new drive to your machine. Whether it's virtual or physical, the first step will always be to attach the drive. This should be self explanatory. 

2) Once the drive is added to the machine, boot it up into your \*nix OS. 

3) Now, you're going to need to identify your new disk. Run fdisk with the following command and look for a disk without a valid partition table. 

	fdisk -l

4) Now, we're going to need to prepare the disk a bit. we'll do this with fdisk to make the disk an LVM disk. Run the following codes (assuming your disk is /dev/sdc): 

	fdisk /dev/sdc

This will open the fdisk prompt and we'll run some commands. (with explanations. Don't run anything in parentheses). 

	n (Creates a new partition) 
	p (Creates a primary partition) 
	1 (Puts this as the first partition) 
	(Just hit enter and accept the defaults for the first and last cylinders) 
	t (This will select the 1st partition, since it's literally the only one) 
	8e (8e will give us the hex value for a Linux LVM. You don't need to know why this is, just know that it is.)
	w (This will write the changes to the disk) 

5) Now that the disk is prepared, we've got to add the disk to the volume group. 

This starts by creating the physical volume: 

	pvcreate /dev/sdc1

Then, using the vgdisplay command, we'll get the names of our volume groups. Let's assume the group you need to expand is called "www"

	vgdisplay

Now, we need to extend "www" to the new drive: 

	vgextend www /dev/sdc1

Once you get the confirmation that the extend happened, you can use pvscan to ensure that the physical volume on /dev/sdc1 is showing associated to www. 

	pvscan

6) We have to start working on the logical volume to get it set extended onto the space within the volume group. We need to confirm that we have the right logical volume to extend with lvdisplay. Let's say the name ends up being "/dev/MainSite"

	lvdisplay

Finally, we extend the logical volume to fill the remaining space: 

	lvextend /dev/MainSite /dev/sdc1

7) NOT DONE YET! Now we have to resize the filesystem on the logical volume to fill the remaining space. To do this, we use the resize2fs command (or xfs_growfs if using xfs):

	resize2fs /dev/MainSite

8) Check your work! Run the df command to enxure that the volume group has all the space it should have: 

	df

## End
Assuming you've done everything right to this point, this is the basics of adding a disk to a logical volume. 

If you have any questions, you can send me an email to jcboysha@protonmail.com.
