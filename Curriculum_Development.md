#Curriculum Development

## Summary
Every last IT professional I have met knows about the SDLC, or the Systems/Software Development Lifecycle. This is extraordinarily important for any well developed system and the maintenance of said system. The same is true for Curriculum, which when viewed holistically is essentially a system. 

## What is the SDLC?
The Systems Development Lifecycle is a set of stages (with hundreds of variants from various developers on which way is best). Effectively, all variants of the SDLC can be construed as expansions on 5 stages. These stages are: 

1) Planning
2) Analysis
3) Design
4) Implementation
5) Maintenance

At the end of the 5 steps, you start back over planning any sort of changes you determine you need to implement from bugs, evaluation, or general system improvements. This sort of lifecycle helps keep systems operating at high levels of efficiency with features that will keep up with requirements of the project, stakeholders, and or users of the system. 

## Why is Change Management Important?
Hold up now, we were just talking about the SDLC and Curriculum Design, how the heck does Change Management play into this? 

Change management is _literally_ everything. In every system, in every action, there are a series of steps people take (generally without realizing it). In any change the following steps are taken:

1) Identify Change
2) Request Change
3) Analyze Change
4) Evaluate Change
5) Plan Change
6) Implement Change
7) Review and Close Change

In this process, generally, people do most of this all internally. They think about what they want to do, they ask themselves if they should, they figure out the consequences, they make a plan, and they make the change. Then, they go through and review the change. Afterwards, they figure out if any more changes need to be done. In a more formal setting, generally, the person requesting the change and the person(s) reviewing the change are different. This requires accountability from both engineers, management, and stakeholders. 

The SDLC and Change Management go hand in hand. A system can not be designed, or changes made to the system, without a holistic change and viewpoint of the entire process. 

## Curriculum Design for IT Nerds.
Well, ok, these all make sense, but now we should take a look at how this applies to curriculum development. In fact, this has all been planned out a long time ago. 

The ADDIE method was developed by the University of Florida in the 1980s based on instructional design paradigms in process by the US Army from the 1970s based on the SDLC, which existed in some form since before the 1960s. The ADDIE method consists of 5 steps. (These may seem familiar)

1) Analyze
2) Design
3) Develop
4) Integrate
5) Evaluate

In these steps, much like the SDLC, you have to go through a series of sub-processes that are generally done in a cyclic fashion. Here's the way this all happens: 

First, a designer analyzes what they need to teach, their audience, and the methods they want to use to get it across (based on audience, generally either pedagogical or andragogical technique). Then they design their master plan, develops specifics for the environment they work in, and (with approval) integrates it into the environment. At the end of this process they evaluate how things went by gauging student learning, retention/application of information by students, the general thought of the students on the material and instructional methods, and the thought of the instructors on the way the curriculum is provided and how well it facilitates their ability to instruct students. 

Effectively, the ADDIE method is the SDLC of Curriculum Design. 

## The Importance of All This
Without the SDLC, the ADDIE method, or good CM practices it is extraordinarily easy to develop a curriculum or system out of order, without foundational requirements, or creating unintended consequences if not well analyzed. Moreover, without proper analysis and design, a developed curriculum may cover all necessary factual elements while leaving both instructors and students confused and unable to properly process the data; especially if data is presented out of order.

At the end of the day, a fully developed curriculum can still end up being poorly developed without proper analysis or design. 
