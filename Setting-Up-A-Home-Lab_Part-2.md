# Setting Up a Home Lab Part 2
## The General Use Network

So, one of the things I want to do with the home lab is build up a bunch of services that will practically get used by myself and my family. Partially, this is because I want to prove that the home lab can be useful (to myself if no one else) for other than just learning. Mostly, this is because if the services are being used regularly, and people start relying on them, it'll become pretty clear if something stops working and I'll be forced to fix it. 

## The Services
I've got a few services in mind: 

1. Media Server
This will be for multimedia streaming to various devices in the house. 
2. Minecraft Server
Because, why not have a Minecraft server? They're straightforward. 
3. A Small Virtual Desktop Infrastructure (VDI)
I have a couple kids old enough to play with computers. One of them is old enough to start learning scratch. I, on the other hand, don't necessarily want them to have their own stand-alone devices. So, my plan is to pick up a couple old laptops and have them boot to terminals to a VDI hosted machine. That will allow them to mess around in an environment where I can COMPLETELY control everything. 
4. VOIP server
I want to run a VOIP server and get everything up and running using a SIP Trunk service. This will give me some experience configuring VOIP for actual use, and will give me a chance to play with VOIP phones. 
5. File Share
I want a central file share I can store stuff that I use on a number of machines on my network. This way, I can work it on the network, or on a cached copy, and sync it when I'm home. 

## The Network
Each of these networks are going to need to be configured separately, and with specific routing rules. For this network I plan to use VLAN10 (on a network of 192.168.117.10.0/24), and I plan to have this entire network routable to my home network. For the moment, I'm thinking I'm going to need to have everything addressed statically. Once I can get that sorted out I'll be figuring out a DHCP server for each environment.

## The Process
So this process is effectively going to be the same for each one of the services, but it's important when planning out an environment and a system to codify the way you're going to do stuff. Think of these like the policies and procedures of a business environment. The difference here is, in your personal environment, you have more leeway to change these off the cuff if they're inconvienient. (Something you will have the time to fix at the end of the process). 

The process consists of a pretty straight-forward series of steps: 

1. Plan
 * In planning you need to define your requirements and think about what you want the service to do. Don't try and list a named service yet (for instance, say "Media Server" instead of "Plex Server" unless you have a really compelling need for it to be Plex specifically). Once you have your list of features you want in the service, list out your must-haves, your nice-to-haves, and your can-live-withouts. This will help you define your acceptance criteria for finding the product you want to use. 
2. Analyze
 * In this step you need to look at competing options for what you want to do. You should always try to do an analysis of alternatives on *at least* 3 different products. This ensures you get a product that might have some added features you didn't even consider in your initial planning. This analysis is done by comparing features that are written out and documented. There's no installation yet. 
3. Get Ready
 * Despite the temptation to jump right into installing, at this step you should start preparing the environment and to install the product. Write up your expected result with installation, write up the process you plan to follow, and write up how you'll roll-back (or undo) everything in the event things go wrong. This is the change management step. It'll involve planning out changes, and reading software documentation to prepare your installation process
4. Execute
 * Now that you have your plan written out, you're change is documented, and you're ready to go it's time to get installing. Gather all of the images and/or installers you need and start following your written process. This is important! When you make a change to the process because of something unexpected: WRITE IT DOWN. Even in a home-lab environment you'll probably find yourself re-installing software and re-doing a lot of work. If you have good documentation it'll be much easier the next time around. It'll also teach you to recognize shortcomings in a process when you're getting ready and proactively correct before you get to crunch time.
5. Summarize
 * Here there are two possible options this could end with: Success and Failure. 
 1. Success - Document what went well, and what you could improve (There are **ALWAYS** things to improve). Keep these documents, and all the notes from the previous steps, in the same place. This way you can not only replicate the process next time you need to, but you can track your growth as an administrator/architect. These can also be useful to take to job interviews and show off. 
 2. Failure - Document what went wrong, and what went well (There are **ALWAYS** things that went well, find them.) Use this summary and the notes from the previous steps to figure out what went wrong and how to fix it. Once you identify the problem, go back to step one and get started again, implement the fixes, ands ee what you can do. Keep these notes! Don't just overwrite them when you finally succeed, they will show your growth as an admin/architect and your troubleshooting process. You can learn more from failure than you can from success (including how to start succeeding). 

There's my usual process. I call it PAGES, makes it easy to remember. Now this is all written out and the plans exist, now it's time to get started working out the home-lab and getting everything started. Stay tuned while I inevitably fail 9 or 10 times to install a media server. 
