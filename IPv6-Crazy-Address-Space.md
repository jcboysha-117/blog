# IPv6 and It's Ridiculous Address Space

IPv6 is to be the next standard in internet addressing. It uses 128 bit addresses. Allowing for a total of 340,282,366,920,938,463,463,374,607,431,768,211,456 addresses. This means that for every square meter of space on the planet Earth we will have 6.6713599e+23 (roughly 670 sextillion) addresses. (This many per Square Meter on Jupiter: 5.5403672e+21 (roughly 554 Sextillion))

To put this into some more perspective the Intel Core i7 processor has 731,000,000 transistors on it. That means that should every transistor on an i7 processor have its own IPv6 address it would take 9.1049356e+14 (or about 910 trillion) processors to use up the full range of IPv6 Addresses. The processors alone (assuming no other connected equipment) would cover an area of 239,460 KM^2 (or roughly making it the 82nd largest country in the world).

But as we all know processors don't function well by themselves. So let's assume you attach these to ultra low form factor computers just to keep up space. Which are about 0.06 m^2 each. It would cover an area of 5.4629614e+13 M^2 (about 5.5 trillion meters square or 5,500,000 KM^2.) Making it, now, about the 7th largest country in the world. Assuming no keyboards, mice, monitors, cabling, or network equipment to make it all work and warrant actually USING the IP addresses. 
