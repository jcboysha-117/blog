# Why Logging Is Important
## and why most people find it useless.

### Common View On Logging
A lot of people think that logging is a giant waste of time. I know, I've worked with many of them. They complain that logging takes up space, there's too much nonsense in the logs, and there's no point in trying to go through them because there no way to separate the good stuff from the junk. 

> "Garbage in, Garbage out. All logs only give you garbage." ~ people I have worked with. 

In many ways they're exactly right. In most environments, on most systems, and in most cases there are just too many logs to filter through. It would be horrendously impractical to go through them all by hand, and even worse to have to go through all of the logs of various systems to correlate the logs from each one to determine if an event on one machine coincided with an event on a different machine. In pretty much any subworld of information work that's just not going to happen. Even if an admin were to try, they'd more than likely miss the incident they're looking for, and just assume there was no correlation.

Because of these known issues there are a lot of viewpoints that logs just hog space. They're rarely used, and even when they are they're never as helpful as people think they will be. When you have all of these problems with logs: no one ever looks at them and they exist solely to maintain compliance with some regulatory standard or company policy. Sadly, all too many a time this is the fate of any number of system logs. 

Luckily there are a handful of ways to mitigate these problems. I'll give you three commonly used techniques in order of least to most helpful. 

### Trim Down Your Logs
The first place a lot of people go to fix their logging problem is to trim down what logs you capture. That's useful... to an extent. It's one thing to trim out repeated instances of the same application throwing the same error, or not log every time a user goes idle a their workstation, but that's *rarely* where this stops. Once the easy to ignore logs are handled, and space hasn't been saved, the useful logs start getting trimmed. I've had issues with systems logging failed user attempts, but not successful logins, or logging when a system shuts down, but not logging over-temperature warnings, fan events, or power fluctuations. 

In the first case, this is a problem when you're trying to determine if there is an intruder in your network. If someone fails to login a thousand times, and then another user fails to login a thousand times, and that keeps going, it is reasonable to think there is likely an intruder attempting to gain access to a system on the network. The issue comes when those stop. Did the attacker gain access? or did they give up? Without knowing when an account *successfully* logs into a system, you have no surefire way of knowing if the attacker entered the system with that account, or if they just stopped trying to attack. It is important to have these logs. 

In the second case, if a machine keeps shutting down, and there's no discernable cause, but the user keeps saying things like "It gets a little warm, and the fan spins up, and then it shuts down", you can probably assume that the machine is over heating. The issue comes when you have to figure out exactly what's going on. Is the fan turning on because of a power fluctuation that's causing the battery to get hot? Is the fan turning on AFTER the processor is overheating (probably an issue with the temp sensor)? Is the processor just getting too hot? Trying to diagnose these problems without those logs is easily doable, but only if you, as a technician or admin, are given a significant amount of time with the machine. If you had the logs, instead of taking a few days to solve the problem (and a couple more to get the part replaced for the user) you could have the solutuion in minutes and a new tube of thermal paste on the way. 

All in all, when you start trimming down logs it's a well-meaning effort, but it rapidly turns into a case of under reporting to save space. 

### Rotate Your Logs
Rotating logs is another big one. It's important to do, but only if done *correctly*. Many an admin have resolved their logging problem by rotating out their logs, and relying on the system backups to be their posterity source of truth. This is a HUGE no-no. When you rotate out logs, you lose data. Most systems that make this easy to do rotate out the logs by deleting old logs every 24 hours or so. This is great for space saving, and really limits what you have to go through. 

The problem is it also limits what is *available* for you to go through. If you have an attacker who gains entry into your system, and you don't notice for 26 hours with a 24 hour log rotation, you are screwed. Either you're going to have to dig into a backup to read the logs (hoping the backup completed properly while the attacker was doing their thing) or you're going to have no idea how the attacker got into your system in the first place. The best way to rotate logs is to rotate them into an archive. It saves on space (at least a little), and lets you keep the logs. But, over time, this will leave you with the same problem that got you rotating logs in the first place: space. These archives are going to start taking up space... fast. 

You could back them up to another server, but then you have to worry about naming conventions of files, storage schemas, etc. Why do that, when there is a better solution that solves *all the problems*.

### Use a log aggregator
I'm not here to sell you on a specific solution (things like graylog or logstash). I AM here to impress upon you the benefits of using a log aggregator in your environment. First, this will get the logs off your local machines. You can rotate your logs **as soon as they happen** if you really wanted to, because they're all going to be sent to the log aggregator. 

So, I may have been a BIT misleading, you're still going to have to deal with space, but you're going to have a dedicated space for storing logs. No more full SQL partitions, no more server slowdowns because of full disks, none of it. You'll have one dedicated machine with sweet, sweet log space.

The way all of this works is simple. First, you set up your log aggregator (follow the guidelines for your chosen log aggregation solution, but make sure you have a couple-few hundred or so gigs raided to keep all the logs). Then you set all of your servers to send their log files to the aggregator. Once that's done, set your aggregator to rotate logs into archives and store them, by time period, instead of sorting them by device, and you're all set. From here you can do super amazing things. 

Since you're not trimming your logs you can do all sorts of fun stuff: want to see if multiple machines in the same server rack saw the same voltage fluctuation at the same time? You can totally just go onto the log aggregator, filter on event type, time period, and location of device and get all of that data in one useful place to make some great information. Need to see if the same account was being used on multiple servers at the same time? Just filter those logs out. 

Even better: since the rotation is being handled by the aggregator, if you need to see what was happening 36 hours ago: *no more digging through backups*! Just import that archive, run your filter, and get your super useful log data. Once you dedicate a useful place to put all of your logs, and have a system to correlate your data, the sky is the limit. Now go forth, be fruitful, and take all of that raw log data and turn it into super useful business information. 

### Questions?
If you want to know more, or want to ask a question, or just want to chat: shoot me a message on gitlab, and I'll figure something better out. 


J.C. Boysha - 06/04/2013
