title: What are networks? How do networks work?
author: jcboysha
description: 
post_id: 2
created: 2014/05/13 15:37:36
created_gmt: 2014/05/13 15:37:36
comment_status: open
post_name: what-are-networks-how-do-networks-work
status: publish
post_type: post

# What are networks? How do networks work?

Networks are a connection of items with some sort of organization. There can be networks of numbers, networks of people, networks of roads, rivers, or (for the purposes of this post) networks of computers. Computer networks are groups of interconnected devices that share data. This sharing usually takes the place in the form of the client-server relationship, it can also take place in a peer-to-peer relationship. Interconnected devices are devices that share a physical connection at some level. In Wireless networks this physical connection is in the form of radio waves. In wired networks the Ethernet cable forms an actual wire connection to the network. Fiber optic cables are also physical networks that use light to transmit data. Computers communicate over networks by sending electrical signals back and forth to exchange data. These data are then converted into the information you see when you access a webpage, for example, or download a file. Networks use standard protocols to exchange data. That is why multiple different types of computer (Mac, Windows, and Linux for instance) can all exchange data over a network. When a computer accesses a website a complex and intricate dance of steps is started over the network. This process takes place in seconds, or less. With how quick everything works it's hard to believe how much actually happens. I'll use a practical example to explain what happens. 


