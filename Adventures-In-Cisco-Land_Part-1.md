# Adventures in Cisco Land. 
## Part 1, the router. 
For the people that know me this may not come as a shock: I'm not the world's biggest fan of networking (in either the technological sense or the business sense). Don't get me wrong, I understand the concepts, I learned what I needed to know, and I know how to secure a network. I'm just not great at building configs for devices or building out a strong network architecture. I had to help reconfigure and learn how to process the configuration of a distribution center once. I got through it, it was a job I was in for a good while, and it was fun. There was a lot of learning. But other than "switchport mode access" and "shut, no shut" (effectively a port reboot), I don't remember much of what I learned. This job was just about 8 years ago at this point. 

This is something I aim to change... 

Something that I can never hit home enough is how much people in CyberSecurity, or any other technical field, really need to focus on continuing to better themselves professionally and academically. As we work through our industry, and its rapid change, the only thing we can do to keep sharp is stay on top of the newest events and ensure that we're constantly broadening our horizons. Our success in our industry is predicated on the ability to leave our comfort zones, get shit done, and come back to the mundane. I recognized, in myself, a lack of understanding and skill (this time in networking), and decided to fix that. So I built up my home lab (another post will be coming about that), and forced myself into a situation that I could only use a phone and an iPad to get this lab on the network. These are the useful notes, configuration steps, and processes I found along the way that were helpful. I hope that these will help you on your journey through networking!

## Erasing a Config
On the 871, at the very least, the process to erase the config from a logical perspective is fairly straight forward: 

`enable`

`<enter password>`

`write erase`

`"Erasing the nvram filesystem will remove all configuration files! Continue? [confirm]" <enter>`

`configure terminal`

`config-register 0x2102`

`end`

One thing I did learn about this: Make sure your ethernet cables are UNPLUGGED (not the console cable, just because it uses an 8P8C connector doesn't mean it's an ethernet cable). Otherwise when it starts up it'll try to network load the configuration. Otherwise, once the process above is written in, all it takes is a reboot of the router and you can go right on back to the default configuration and get started (or keep working if you're like me and have had to do this 800 or so times). 

## Initial Config
I found that running through the initial configuration dialog (wizard, thing) was helpful when building out from scratch, so I try to do that. 

`"Would you like to enter the initial configuration dialog? [yes/no]:" <yes>`

`"Would you like to enter basic management setup? [yes/no]:" <yes>`

These two pieces let you start the most basic of basic setups. I'm gonna go through this way until I figure out exactly how all of these configs and pieces work together. 

`"Enter host name [Router]:" <name of router>`

Name your router something memorable. I'll do a post on naming conventions in systems and networks at some point, but for a small lab this is less important. Have fun. 

`"Enter enable secret:" <secret>`

This is the password you will use to enter privileged mode on the device. This is akin to getting super-user access to a terminal. Be careful with it. I use a standard password convention in my lab environments. I'll do a section on that when I write up the "setting up a home lab" post. 

`"Enter enable password:" <password>`

This is the same thing as above, but there for compatibility. 

`"Enter virtual terminal password:" <password>`

This is the password used for accessing the router over network interfaces, vice directly consoled in.

`Configure SNMP Network Management? [yes]:` <answer>

This will configure the Simple Network Management Protocol interface on the device. I said yes, it's up to you how you want to go about this. 

`Community string [public]:` <string>

This is like a light secret that will allow one to actually retrieve SNMP data from the device. When an SNMP request is sent, it'll contain the string. If the string is wrong, then the request is dropped. 

`Enter interface name used to connect to the management network from the above interface summary:" <interface choice>`

This one honestly confuses me a bit, and I'm not entirely sure what it does. From my research, though, I've come to the conclusion that this is supposed to be answered with the VLAN interface that will be used to manage the router on the network. I think. I know what most of those words mean, and I kind of understand them in that order a little bit. 

`Configuring interface <chosen interface>`

`Configure IP on this interface? [no]`

This sets the ip of the VLAN interface chosen. I say no for right now to do this later. 

At this point the wizard thing is done, and you'll want to save the configuration and exit. This will give you a default interface with almost all of the physical ports shutdown and ready for config. This is almost exciting!

##Understanding ports
In the context that we're discussing here, the ports I'm going to be talking about are /physical/ ports. So, when I mention port configurations, or switchports, or any of that, I'm specifically discussing the FastEthernet ports that are present on the device. I don't think anyone would confuse these with logical ports, but just in case. (For you networking nerds who might be reading this, Layer 1 ports, not Layer 4 ports.)

## Configuring NAT
Eventually, I want to have a number of networks (one each for Malware Analysis, R&D, Hack lab, and General use). For right now, however, I just want the router to be able to NAT internet to the switch on the other side (since that also has to be configured and I'm excited to get the lab online. Everything else can come later. This, in general terms, requires setting up an "outside" adapter and an "inside" adapter. 

The "outside" adapter is going to be the interface that will connect to the WAN connection (in this case, FastEthernet4). Generally, the inside adapter is going to be the adapter that will share this connection, in my case, Vlan1. 

`enable`

`configure terminal`

`interface FastEthernet4`

`description WAN interface` 

This last command will set a description for the adapter. That way, when I come back to this in 6 months I can see what this interface is supposed to do. 

`ip address dhcp`

This will have the adapter get a DHCP lease from my primary router (the one that I was given by my ISP), and I won't have to worry about mucking with that PoS to set up static IPs. 

`ip nat outside`

This command will set up FastEthernet4 as the outside adapter for my NAT scheme. 

`no shutdown`

This will enable the FastEthernet4 port. 


`interface Vlan1`

`description Primary Vlan`

`ip address 192.168.117.1 255.255.255.0`

These commands will set an IP address (important since it's going to be the gateway of other devices on the network), and give the Vlan a description. 

`ip nat inside`

This command will set up Vlan1 as the recipient of the NAT being shared out by FastEthernet4. 

Now, from my understanding all the ports on the device are bound to Vlan1 by default. So, at this point, we need to configure an adapter to connect to a host to start testing this configuration. 


`interface FastEthernet2`

`description Client`

`no shutdown`

These commands will enable FastEthernet2 and give it a description. 

At this point we need to plug cables into all of the ports on the device. Port FastEthernet4 gets connected to the ISP router, and FastEthernet2 needs to get connected to a test client. On the client, I set an IP of 192.168.117.2/24 with a Default Gateway of 192.168.117.1. Running a ping to the gateway gives good response! Finally, network connectivity... at least to the router. At this point, however, the router doesn't have any way to route traffic to the internet. 

Since I'm going the route (haha) of just getting on the internet, let's set up a route that directs EVERYTHING out FastEthernet4. This, in theory, will allow internet connectivity to pass through the router... I hope. 


`enable`

`configure terminal`

`access-list 1 permit 192.168.117.0 0.0.0.255`

This will create an access list that will allow all trafic coming from the 192.168.117.0/24 network. 

`ip nat inside source list 1 interface FastEthernet4 overload`

Ok, this is a thing. It's going to set the IP NAT for inside with a source for addresses from the Access Control List (ACL) 1 (that we just set up) on interface FastEthernet4 and overload it so multiple addresses can get connected through this port. (It's actually using something called PAT, but we'll go over that another time). 

`ip route 0.0.0.0 0.0.0.0 FastEthernet4`

And this should create a route that allows pretty much EVERYTHING to just pass through. Got everything connected, ran my ping to the router (that still worked), it did not get me to the internet. At this point, I know I have a passable configuration, but something is preventing me from getting to the internet. Now, I'm reaching out to a friend of mine (a network engineer I work with), and trying to see what to do from here. The first recommendation was to run the command: 

`ip routing`

This turns on routing for IPs (I'm guessing). It did not work as intended. The next recommendation was based on the outputs of 

`show interfaces brief`

`show ip route`

These commands show a brief list of the IPs and their configuration, as well as a list of routes that traffic can take. This lead to a problem with my default route. I was routing traffic through a port instead of an IP. ultimately, the fix for the above was simple! 


`no ip route 0.0.0.0 0.0.0.0 FastEthernet4` 

This undoes my earlier broken route

`ip route 0.0.0.0 0.0.0.0 10.0.0.1`

These set the default route to the ip of my outside router (10.0.0.1) instead of the outside port (FastEthernet4). 

# The final Config
Alright, the end has come and I can get on the internet on my client. Now, to kind of undo all my work and plug the working port into my switch and see what I can do about getting that configured. 

The Config: 

`Current configuration : 1270 bytes`

`!`

`version 12.4`

`no service pad`

`service timestamps debug datetime msec`

`service timestamps log datetime msec`

`no service password-encryption`

`!`

`hostname <host name>`

`!`

`boot-start-marker`

`boot-end-marker`

`!`

`logging message-counter syslog`

`enable secret 5 <secret>`

`enable password <password>`

`!`

`no aaa new-model`

`!`

`!`

`dot11 syslog`

`ip source-route`

`!`

`!`

`!`

`!`

`ip cef`

`no ipv6 cef`

`!`

`multilink bundle-name authenticated`

`!`

`!`

`!`

`!`

`!`

`!`

`archive`

 `log config`

  `hidekeys`

`!`

`!`

`!`

`!`

`!`

`interface FastEthernet0`

 `shutdown`

`!`

`interface FastEthernet1`

 `shutdown`

`!`

`interface FastEthernet2`

 `description client`

`!`

`interface FastEthernet3`

 `shutdown`

`!`

`interface FastEthernet4`

 `description WAN Interface`

 `ip address dhcp`

 `ip nat outside`

 `ip virtual-reassembly`

 `duplex auto`

 `speed auto`

`!`

`interface Vlan1`

 `description Primary Vlan`

 `ip address 192.168.117.1 255.255.255.0`

 `ip nat inside`

 `ip virtual-reassembly`

`!`

`ip forward-protocol nd`

`ip route 0.0.0.0 0.0.0.0 10.0.0.1`

`no ip http server`

`no ip http secure-server`

`!`

`!`

`ip nat inside source list 1 interface FastEthernet4 overload`

`!`

`access-list 1 permit 192.168.117.0 0.0.0.255`

`!`

`!`

`!`

`!`

`snmp-server community <string> RO`

`!`

`control-plane`

`!`

`!`

`line con 0`

 `no modem enable`

`line aux 0`

`line vty 0 4`

 `password <password>`

 `login`

`!`

`scheduler max-task-time 5000`

`end`





