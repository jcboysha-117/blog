# Adventures in Cisco Land Part 2 
## The Switch and the AP

After I got the router working, I decided that wasn't even close to enough yet! So, I plugged it in to my switch, and went from there. I have learned 3 things in the last 3 hours about Cisco devices.

1. Switches will work just fine from a default config (queue the cringing of every network engineer I know). 
2. The APs are a pain to get factory reset. 

## Hardware Information
It occurs to me that in my last post I talked, a number of times, about my "setting up a homelab" post. First, I'm realizing that this is out of order and I should have done that post FIRST. Second, I didn't list any of the hardware I'm using at the moment, so let's get to that a bit: 

1. Cisco 871 Router
2. Cisco Catalyst 2960 Switch
3. Cisco Air 1242 AG Lightweight/Autonomous Access Point

## The Switch
For the moment, I just have the switch at a standard configuration. Everything is bound to Vlan1, everything connects to everything else, it automagically works. I'll be doing more with this as time goes on and start building out Vlans. But it just works for now. 

## The Access Point
I want to be able to access the network wirelessly while I'm working on stuff. This will let me connect to things from the comfort of my couch instead of my less-than-stellar lab chair. 

I got it plugged in, and connected. It wanted a username. This is what I get for buying lab equipment on Ebay. 

I ended up resetting the AP by holding down the mode button for 6 seconds while powering on. The process was: 

1. Unplug AP
2. Hold down mode button
3. Plug in AP
4. Wait until the ethernet light turns a reddish-amber color
5. Release mode button

Now were cooking with Cisco! (haha) Or not... still asking for a username... What the hell could this thing want from me? Google has all the answers right? To the Google! After about 10 minutes of Googling, I came to the conclusion that the default credentials are Cisco/Cisco. So, that was a useful thing to know. 

`username: <Cisco>`

`password: <Cisco>`

`enable`

`secret: <Cisco>`

It's important to know that, at this point, everything has gone smoothly. And that Cisco for the enable secret DID work. 

`configure Terminal`

`% Invalid input detected at '^' marker.`

Ok, so at this point I am very confused. Every Cisco device I have ever used uses the same Configure Terminal command! What gives?! It turns out that it's fairly likely that I have an AP set up with a controller-based image. This is going to be a problem, I think, since I need this to be a standalone AP, but for the moment I just need to get myself a configuration terminal so I can set a static IP to the FastEthernet port and see what happens. This won't be necessary once I get my DHCP server stood up, but since I wanted to do all of the networking first, static it's going to have to be. 

I did some more googling and came across some useful commands. Namely: 

`debug capwap console cli`

At this point, I usually explain what the commands I run do and all that. (Former students of mine, take note, I'm not practicing what I preach here). I have absolutely no idea what this does, but it came from the Cisco learning forums and it looks like it allows for console-based debug commands to run. I typed it in, and it worked. 

`Configure Terminal`

Boom! Terminal!, now to turn off all of the logging. What you don't know (because I haven't told you yet) is that this whole time every 10 seconds a message has been popping up on the CLI talking about the AP not having an IP address. I need to get that turned off. I really only know one nuclear way to do this. 

`enable`

`configure terminal`

`no logging console`

This makes it so no logs will print to the console. It's going to hide everything, even the things I might need to see... I can always turn it back on later if I need. Now that's done and I can see what I'm typing, it's time to set a static address

`enable`

`configure terminal`

`interface FastEthernet0`

`ip address 192.168.117.245 255.255.255.0`

This has set the IP address on the ethernet port, and it's time to see if the device is capable of seeing the internet. 

`enable`

`ping 8.8.8.8`

SUCCESS! Now to set the essid and security level of the AP so I can get connected to it and hit my whole environment from my couch without a long ethernet cable. 

(As an aside, I set a static route on my Manjaro laptop to test the switch configuration. At the moment, I can communicate with everything in the lab from my home network. Which is just fine for right now). 

As it turns out, all of this works so far, but I can't write to the NVRAM, and there are some other issues forcing me to have to upgrade IOS. Something I can not do without a service contract or a CCIE. While I figure all that out, I'm going to go back to switch configurations and start prepping out VLANs. That said, the next post is going to start my series on building out and setting up my home lab! Stay tuned. 




