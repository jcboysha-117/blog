# Using Forensics on a Daily Basis

In my field I often run across times that I have to do forensics of some kind, recovering data, investigating a breach, or determining malfeasance of employees. In my personal life, however I run across times where these skills are useful far more rarely. 

## Some Background
The Nintendo 3DS uses an SD card for storage of purchased digital games, some data storage, and other information. The system, by virtue of needing to modify files to save games, will not load the games on a system where the fielsystem on the SD card is set, either by hardware or software switching, to read-only. In this case, my son (5 years old) brought me his DS looking for the "walking Mario" (as distinct from "driving Mario"). 

This game came _with_ his DS. So the only place we had a copy was on the SD card that came with the DS. (I want _no_ lectures about backups here :)). Something happened that caused this card to be corrupted or have a short that was causing the write-block to be enabled on the card. 

## The Forensics
Often in digital forensics you have to take a complete bit-to-bit accurate copy of a disk. This allows you to have two things: Best Evidence, and Operational Evidence. These two pieces together form the evidence that you can operate on to prove a case and the evidence you can manipulate to find all of the facts the evidence can impart. 

Best Evidence is the copy I was referencing above. This is a completely untouched, very carefully tracked, bit-to-bit accurate image of a disk that is stored in a controlled space, preferrably with a complete log of people who have had access to the drive. 

Operational Evidence is a second bit-to-bit accurate copy that an investigator takes ___after___ taking the Best Evidence image. (This Best Evidence is turned over for independent verification, submission to court, etc). 

Generally, you go through and perform a number of carefully documented steps on the Operational Evidence, in this case I didn't need to do that, so back to my story. 

## The Situation
In this case, all I needed was a bit-to-bit copy of the card copied to another card that would store the data (and any associated DRM, signatures, etc) while still allowing for writing. In this case I did what is called a disk duplication. 

There is a command built into most \*nix distributions called "dd". (If that seems familiar to "disk duplicator" it should). This command takes an input location (if) and an output location (of) that will take an bit-to-bit image of a disk, or restore an image to a disk bit-to-bit. 

In this case I used dd to copy the SD card to an image, restore the image to a new SD card, and *boom* my oldest was off playing "walking Mario" again in minutes. 

Yeah, that's pretty much the whole story. But it DOES show how even something as esoteric as digital forensics can be used in daily life. 
