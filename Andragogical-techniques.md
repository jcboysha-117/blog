# Andragogical Technique. 

This is going to be a bit out of my norm for blogs. In addition to a security generalist, broadly skilled IT pro, and generally a nerd I am also an educator. While I currently work as an instructor as one of my primary roles, I have been working with instruction, development, and training for employees and adults for over the last 5 years of my career. A big consideration, when working with workforce development and adult training, is the difference in how Adults and Children learn. The way children learn is generally referred to as pedagogy (from the Greek "leading children"), the way adults learn is andragogy (from the Greek "leading man")

## Why does this matter?
Well, adults don't learn like children do. Where children have a limited foundation that instructors work to build and develop, adults have an entire lifetime worth of knowledge to pull from. This will color and affect every last thing you give them to learn. If you stand at the front of a room full of adults and perform a didatic lecture they generally won't take a lot away from it. 

## What do I need to know?
This is slightly easier. There are a handful of principles to consider that generally inform an adult's motivation to learn. 

Adults almost always have a need to know the reason they want to learn something. This will engage them, and give them an intrinsic motivation to learn. This intrinisic motivation can be reinforced by explaining to them how the knowledge is or will be relevant to their work or personal lives in the immediate term. Adults learn best when they're involved with the methods of instruction (back and forth banter, determining what they want to learn, helping the others around them learn the topic, etc.), and when they're solving problems. Moreover, it is important to consider that any foundational knowledge adults have will affect their learning, including erroneous foundation (These need to be identified and overcome). These methods are generally considered in Knowles' theory of andragogocial techniques. The six motivations are: 

1) Need to know

2) Foundation

3) Self-Concept

4) Readiness

5) Orientation

6) Motivation

Additionally, there are a handful of assumptions you have to make about adults in a learning environment. First, they have to want to learn and will only learn what they think they need. The goal of the facilitator (vice instructor) is to ensure that the learner wants to learn, and understands why they need to learn the topic at hand. Adults generally learn by solving problems directly. As I wrote earlier, adults don't take much from a didatic lecture, they take far more from a shared solving of a problem or a conversation about a topic. As the facilitator, you will need to go through and ensure that the learners are engaged. Ask questions, engage with them, ensure that they engage with you. Use the steps in Bloom's Taxonomy (Remember, Understand, Apply, Analyze, Evalute, Create) to ensure that the learners are learning the topic, and have a complete grasp on the topic being covered. 

Experience will color the way the learner interacts. Bad classroom experiences and erroneous foundational knowledge will generally negatively affect the experience while the inverse is true for good and strong foundational experiences. It is very important, early on, to understand what your learners have experienced in the past, both from a general and specific point of view (class and student). Finally, adults aren't children; they need less structure. In fact, adults learners tend to learn better in an informal conversational situation, and when they feel like they are equals in the process. As a facilitator, you have to remember the people you are talking to, the people whose education you are facilitating, are generally skilled in their own field, some will be experts looking for more knoweldege, or looking to change fields. Treat them as such, engage with them and don't just talk at them. Ask them what they feel like they need to know, work with them to foster learning. 

The big take-away (as if I haven't beaten this horse to death and then continued) is that adults will learn better in a conversational setting with back-and-forth than in the context of being talked at. 

## Conclusion
This is just my take on things, right? There are entire bodies of study that end in PHDs, PSYDs, and ED.Ds in Andragogy. If you're interested go out and learn more. Get a better understanding of your own. Try new techniques. Evolve as a person. 

That's really what all this is about: Learning. 

### Random Fun Fact: 
Techniques for Autodidacticism are called Heutagogy. 
