title: Net Neutrality Must be Saved
author: jboysha
description: 
post_id: 95
created: 2017/11/26 00:05:00
created_gmt: 2017/11/26 00:05:00
comment_status: open
post_name: net-neutrality-must-be-saved
status: publish
post_type: post

# Net Neutrality Must be Saved

There are many things about Net Neutrality that are important, not the least of which is the unrestricted access to information. Imagine, if you will, a nation where there are no regulations ensuring that Internet Providers give equal access to all internet services. In this nation, there are tiered internet packages: You have to pay more for Social Networks, Entertainment sites (Netflix, Hulu, etc.), and other categories of internet with only "Basic" internet covered in your initial monthly cost. Now stop imagining: This happened in Portugal. The image below are your different options for internet in Lisboa. ![](https://pbs.twimg.com/media/DNGlrABUIAAr9RO.jpg)   Sounds ridiculous? But that's exactly the type of marketing companies want in the *AAS (Everything As A Service) days. They can charge you more and more for different access, and segment it such that it GUARANTEES the most income. Here's are some, perhaps, scarier implications: 

  * Consider that to know what websites to block, your ISP has to know what websites you're visiting. Think about that. More than they do already, ISPs will keep record of EVERYTHING you do so they can block it, plan out new pricing tiers, and ensure that you are paying them as much as possible.
  * Maybe you live in an area (a painful minority of this country) where you can get more than one ISP. GREAT! Fed up with Comcast? Just google the phone number for Cox... except now Comcast can block that page from ever loading, or, worse, inject an incorrect phone number or inject content to prevent the phone number from displaying. No more competition. On Comcast and have Netflix? No more Netflix for you UNLESS you also either pay a significant fee OR subscribe to cable (defying the point of Netflix).
  * What about if the ISP set up a "porn" plan? And sent you an itemized bill that ended up in a neighbor's mailbox? Or was maybe in a different kind of Envelope if your ISP doesn't necessarily approve of that kind of thing. (Despite the DIFFERENT laws they'd be breaking there).
These are just basic things they can do as well, there is a litany of more dangerous things they're capable of doing without Net Neutrality protecting the internet. The long and short of it is, Net Neutrality protects us. The U.S. Federal government is, at best, incompetent, and, at worst, blatantly ignorant, but ISPs are greedy and malignant. Even still I'd rather have Government regulations protecting consumers from the grubby paws of awful ISPs than trust the ISPs to be good people. The other option is to make a new internet... with redundancy and freedom.