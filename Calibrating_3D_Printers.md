# How to calibrate a 3D printer

## Summary
So you have a new 3D printer, everything is working, the belts have been tightened, and you print a nice cube to test it. Except, it kind of didn't come out a cube... in fact, none of the 3 dimensions match in size at all! What could you have done wrong?! Well, you didn't do anything _wrong_, in fact, it printed! What you didn't do, is calibrate it. 

This post is going to summarize the steps necessary to get a 3D printer fully calibrated in 3 dimensions. For this, I'm using the Trovole A8 clone, and a Fedora workstation. Any comparable devices should work just fine. 

## Pre-Requisites and Requirements
First, there are a few things you'll need. 

### Real world things:
You're going to need the following tools: 
* A measuring device, in my case, I use a Vernier Caliper. Digital calipers, dial calipers, even a precise ruler and a good eye will work. (Although, I suggest calipers)
* A pen and pad, marker and white board, stylus and clay tablet. Anything to write on. There's going to be note taking and math involved.
* An assembled and printing (albeit badly) 3D printer. 

You're also going to need to prepare you're computer. If you're like me, I got a printer that doesn't need a computer attached. Eventually, however, you're going to need to plug it in and set some values. For that, you're going to need the following: 
* A computer running Mac, Linux, or Windows. (I'm looking at you FreeBSD users. You're going to have to figure some of these steps out yourselves). 
* A copy of Repetier Host for your chosen operating system (This can be had online for free.)
	* https://www.repetier.com/download-now/
* A copy of Ultimaker Cura (to do the slicing. Yes I know you can use Slic3r, but I like this way better.)
* A USB cable to connect to your pritner, and a port to stick the cable in. 

## Step 1: Get some Values
First, plug the printer into your computer with the USB cable. From here, open your copy of Repetier Host and get it connected (the connect button is in the top left corner of the window). From there, go to the right hand panel and look for a tab labelled "Manual Control". At the top of that tab there will be a place to run G-Code commands manually. In this box type the command:

	M500

This will put a lot of data into your log at the bottom of the screen. You're going to look for 3 values: X steps/mm, Y steps/mm, and Z steps/mm. Take note of these, because as we do the calibrations, we will need them later. 


## Step 2: Print the Calibration Cube
This should be the easiest part, download the STL for the 20mm labelled calibration cube (https://www.thingiverse.com/thing:2543469/files). Once you have this download, open it in Cura and using the settings for your printer get it sliced down to GCode. Once you have that done, either by using RepetierHost to control the printer, or moving the file to the printer by removable media, get the file printed. While it prints, go watch an episode of The IT Crowd, or a few episodes of The Guild, or something. Maybe have a nice cup of coffee.

## Step 2: A Little Bit of Measuring, A Little Bit of Math
Once the print is done and you've gotten it off the bed, you'll need to do some measuring. Take your calipers, and measure each dimension IN MILLIMETERS (if your units are wrong this will end very poorly). Take note of these dimensions. You'll notice that the 20mm cube has an expected measurment in all dimensions of, you guessed it, 20mm. If your measurement is not 20mm (it probably won't be), we'll need to do a bit of math. 

**Note** I recommend calibrating only one dimension at a time, although you can calibrate multiple dimensions simultaneously.

The goal here is to inform the printer of how many steps of the motor actually equal one mm in real world movement. Luckily, a little bit of simple math (thanks to Geometry) will get us going in the right direction: 

	(Expected / Observed) * Current = Steps

What you're going to want to do is divide the expected measurement (20) by the actual measurement you took and multiply that answer by the current number of steps. For example, if your current Y steps/mm is 105 and your observed measurement is 18.2765 you're going to do:

	(20/18.2765) * 105 = 114.9016496594

So we're going to have a new Y steps/mm of 114.9016 (I usually round to four digits, you can be as precise as your printer will let you). Once you have your value(s) for the dimension(s) your calibrating, it's time to set the values.  

## Step 3: Setting the New Values

To set this on the printer you're going to need to run two (2) G-code commands in the same place you ran the M503 earlier. In this case you're going to use the M92 command to set the value, and the m500 command to write the value to the printer.  

        M92 Y<new steps/mm>
        M500

This sets the value of Y steps/mm to the new value. The same can be done for X by running the commands: 

        M92 X<new steps/mm>
        M500

or Z by running the commands:

        M92 Z<new steps/mm>
        M500

Once you have set your new value(s), move on to the next step (endless repetitions)

## Step 4: Rinse and Repeat
Congratulations! You've made it through the hard part, now print the cube again and start over. This will make sure your calculations were right and you'll be able to print a nice fine cube. (If you're doing it my way, you're only hoping for one of the dimensions to be fixed, and the other 2 to need calibration still). Keep doing this until all 3 dimensions come out as a crisp clean 20mm. 

## Step 5: Print Some Cool Stuff
Now's the fun part: Print things. Whatever you want. Print modern art, print non-spherical shapes of constant width, or just print a 3D rendering of your dog! (Maybe even more 20mm cubes, just because you can). The sky is the limit (it's funny because you can actually print a drone). 

## Common Issues
### RepetierHost gives "Permission Denied" when trying to connect on Linux
I found that when running RepetierHost as a non-root user (SHAME ON YOU ROOT USERS) you have to set the permissions on your serial port for your user. First, list the contents of /dev/ and look for a ttyUSBx where x is a number (usually 0). Once you have your ttyUSB value run the following command (replace 0 with whatever you found):

	sudo chmod a+x /dev/ttyUSB0

This should resolve the permissions issue. 

### My Print Layers are Shifting
In my experience this is usually a belt tension problem. Give your belts a tighten and see where it goes from there.

