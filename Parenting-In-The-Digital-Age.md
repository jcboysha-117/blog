# Parenting In the Digital Age

## TL;DR
So, a quick blurb: this is new for my blog. I usually write documentation and reports with an Abstract and Executive Summary at the top. I've found that this helps me figure out what I need to cover. I write it first (despite every last resource on technical writing I've ever read saying not to) and use it as a guide. 

Parenting in a digital world is hard as hell. How do you balance privacy, good digital citizenship, teaching kids to understand the internet, and the need to understand and control what your kids are capable of? I'm going to try and tackle some of these problems in this post. 

## The Background

I'm an HUGE privacy advocate. Like giant advocate of privacy. I get annoyed when ticket checkers on the train check my tickets because I feel
like it's a violation (even though I truly understand the why). That said, the hardest thing about my life so far has been weighing parenting against my belief that everyone, *everyone*, ***EVERYONE*** has an intrinsic right to privacy. That includes my children. I've seen a lot of posts and apps recently popping up that allow parents to do such things as: 	
	
	* Disable their kid's devices if they don't answer the phone
	* Copy all of a kid's passwords
	* Require their kids to empty their backpacks (This post is not just digitally focused)

As a human being, these things infuriate me. Yet, as a parent I can feel some resonance with these concepts. I've been fighting with this resonance and struggling with it for the last couple months, and I wanted to get the ideas I had down, so they might help people more than just me. 

First, each of these things people are doing are trying to teach some basic concepts (respectively):
	
	* Good Digital Citizenship
	* Understanding the Internet (More generally understanding risk)
	* Controlling Access to content and items (digitally and physically)

Those are increasingly important topics to ensure that children understand (think the digital equivalent of "Stranger Danger"). That said, the ways people are teaching them have some inherent problems, conflicting messages, and in some cases teach the opposite of good behavior by way of teaching good behavior for the wrong reasons. Another note: I'm going to stop referring to the targets of the behavioral learning here as people instead of children. Children, contrary to some people's beliefs and actions, are people just like adults and should be treated as the intelligent creatures that they are. 

## A Sidebar about Development and Understanding
Human beings don't really learn facts. We memorize facts, we learn concepts. If someone is conditioned to parrot a behavior as a response to an external stimulus, that's not learned. They don't understand the importance of the behavior, it is (redundantly) conditioning. This is kind of bad when learning foundational behaviors (like those above). This would be like building your house on a swamp and hoping it doesn't sink, all of the behaviors that build up on these are going to be flawed. This concept will be kind of important to understand coming up. 

Also, to make the lesson stick, after the punishment have them sit down and either talk to you (if the child is too young to really write a long paper), or write a paper (1-2 pages) about the punishment, what they did wrong, the importance of the good behavior, and why it was beneficial that the behavior be corrected. 

## Digital Citizenship
Teaching a person that if they don't behave the way you want them to, when you want them to, and attempting to control their life because of it is not good teaching. It's not even good conditioning; it's negative conditioning. This teaches that person to live in fear of not behaving correctly.

### Scenarios
Let me paint a picture for you: Laying on the ground is a teenager. She is unconscious because she got clipped by a passing cyclist late at night. The cyclist kept going and no one is around to find her. You call the teenager and she does not answer. In a sense of misguided well-meaning you disable the phone to "teach them a lesson". They come to and are confused, scared, and do not know where they are. They instinctively reach for their phone and it is disabled. All they want to do is call you and get some comfort and be told that help is on the way. They can't do that. 

Two things have come from this tale: first, a teenager is now stuck in the middle of a city at night with no way to get help but screaming. Second, this teenager doesn't trust you any more. In all the well-meaning effort to teach good digital citizenship, two negative responses have been conditioned into this individual. 

Let's go to another scenario: Your oldest is a college student now. They are in class and taking an important exam (they have not felt the need to share their detailed schedule with you as they are an adult, but still get some financial assistance from you, such as paying for their phone). This phone is necessary for their on-call work as a nursing assistant. You call them to share some good news. They, knowing the fear of having their phone disabled and the need for their phone to literally affect lives (remember their job) , step out of the room to take the call. They find out it is unimportant, but the damage is done. The professor fails them the exam for leaving the class. Because of this failure, they lose their scholarship for the semester. 

The result of this scenario is that an adult in an already struggling class  of people (college students) now has the additional financial burden of a semester of school they were not expecting to pay, and they are now constantly weighing the stress of whether or not the phone might ring during an exam. (Or that they are now required to share with you their personal calendar, which is also unacceptable (PRIVACY FOR ALL)). 

### The Concept
There is a way around this: teach concepts in other ways. Sit down with the person and discuss with them situations and how to handle them. Additionally, think of punishments that DON'T involve disabling a person's communications with other people. Some ideas: 

	* Downgrade their phone to a dumb phone. These can be had pretty cheap now and work on modern networks (Nokia recently rereleased the 3310 for instance). I like this option, it doesn't fundamentally change their lives, but makes things very inconvenient. 
	* Ground them. Don't let them leave the house for a few days, better would be a punch card that says they miss the next X social gatherings they want to go to. They can live their lives, but miss out on the big fun parts. Make it clear that there are consequences to actions that might have short term affects, but don't instill fear into their lives. 

This can help teach the lesson, but requires that you explain the concept and helps them understand the concept. It doesn't instill fear or anger, it's (eventually, once the concept is processed) understandable even to the person punished. 

## Understanding the internet
So, sending your kid out to be on the internet is completely and utterly TERRIFYING. I know, I'm approaching that point rapidly. That said: DO NOT TELL YOUR KID TO GIVE YOU THEIR PASSWORDS. If you absolutely 100% for any reason need access to their account, make them put the password in with you there and have them sit with you while you go through things. There are two reasons for this: 
	1. You are effectively teaching children that it's OK to give people your passwords. It is never OK to do this for any reason. It doesn't matter the position of authority the person requesting the password is. Don't do it. Fight that unless there's a subpoena or a court case involved, and in those cases, request that you can put the password in and change it to something temporary that you will give the authority. 
	2. People should teach children that they have their own space. They control some parts of their lives, but they will have to share (SHARE) access with you occasionally. 

In the real world you will occasionally be requested to set a temporary password or put your password in for an authority figure or a technician. That's ok, giving away your password is not. You, by teaching them that it is OK to share their password with anyone (even you as a parent or a spouse), and it's not. There's an element of custodianship here. Eventually you will probably work for a company that your family does not work for (and even if they do they may not have your level of access). This relationship between you and the company has a level of data protection that you are responsible for, even to protect from your family. Not only that, there may be contacts to personal emails that are semi-confidential that shouldn't be shared. Requiring a child or significant other or anyone at all to share passwords is abusive, dangerous, and sets a super dangerous precedent. Don't do it. 

This conversation focused on Risk, but we're going to address the other part of why parents do this in the section on Control Access. 

## Controlling Access to Content and Items
Related to the aforementioned DON'T VIOLATE PRIVACY discussions, people should have a sense of security in their belongings (the fourth amendment of the United States Constitution exists for this reason). Now, that's not to say you can't help control this. First, the backpack scenario is super easy. Sit down with the person, and go through it TOGETHER. If you're going to read their diary/journal you can go through it TOGETHER. If there's anything super private, they can summarize it for you. If the summary isn't sufficient, THEN you can assert authority with probable cause. This will instill that there is a right to privacy that is protected until there is a justifiable reason to violate it. 

Now with controlling access to content: This is REALLY EASY in today's security environment and it hinges on a commonly available login security: 2FA or two factor authentication. 

If your child has an account you want to control how much time they spend on it, or control when they access it, you can set up 2FA such that they maintain the username and password (the what you know). This will give them complete control over access (you can't do anything with the second factor without the primary factor), but without the secondary factor the login process can't complete. Just say "hey, I'm going to hold your WoW authenticator and you will have to authenticate through me when you want to play WoW." This is reasonable and doesn't give you any access to their accounts, but a semblance of control. The big thing here is this may be cumbersome to you: It should be. You have become a responsible custodian of account for this individual, you will need to be available if they request that access. So, consider how granular you really want to get with this. 

## Conclusions
Pretty much, parenting in the digital age is going to be hard; maybe harder than generations before (I don't know, I wasn't a parent then). That said: there are ways to do all of this responsibly and raise digitally smart kids instead of digitally controlled kids. 