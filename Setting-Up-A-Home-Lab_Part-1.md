# Setting up a home lab
## Part 1: The Planning

So, I recently, to the shock of some, got laid off from one of my jobs. I was teaching, now I'm not. I was contracted out to finish my semester, and found other work. What this meant for me, however, is less time and resources to do research and keep my skills as sharp as I would like. I work two jobs, I will continue to work two jobs. Both of those jobs, however, rely on my skills staying sharp. So, I've been working two jobs and getting Master's degree for the last year. Now I'm going to work two jobs and have time to do my own projects! It's exciting. 

To do these projects, expand my skills into new areas (like networking), try out new PoCs I see, analyze malware, and generally have a good time doing stuff I can't do in production environments, I need/ed an environment. Well, I have the hardware... Now I just need to build out the environment.

## The Hardware
At the core of any home lab is going to be hardware. There are a lot of ways to build out a lab. I'm, especially for starting professionals, a HUGE advocate of the virtualized lab. Pretty much everything you need to be able to do early in a career, or starting out in learning, can be done with a bunch of VMs hosted on a single hypervisor. Generally, these can even be hosted on a laptop or desktop that also serves as a daily driver. That said, there comes a time when professionals should be comfortable getting their hands dirty with hardware, building things out, planning infrastructures, and preparing security for distributed networks and systems. Usually, people reach this level in a professional environment and using a combination of training and company-provided equipment. (At least, that's how I did it, and that's how most people I've worked with have done it). So, to replicate environments I work in, and start working on the other side of things (for me this is networking), I opted to go for a full hardware lab. This will let me play around with all aspects of the environment (cabling, network management, systems management, building out new devices, etc). 

/Sidebar: for me this is super fun specifically on the networking side. I'm not sure the network engineer I work with most would be keen on me learning this stuff on his network... and I KNOW there are things I want to try that he wouldn't be keen on me doing with the network. (VLAN hopping anyone?)/ 

So, at the end of the day, I decided to effectively build out a miniaturized professional environment. I put the following hardware in: 

1. HP EliteDesk with 2x1TB WD Blues, 32 GB RAM, Server 2k16 with Hyper-V, and an i7. 
2. HP DL380 G5 with a hodge-podge of hard drives for the moment (planning to get these sorted), 16GB RAM (upgrading soon), ProxMox VE, and 2 Xeon 5160s
3. HP DL380 G5 with no HDDs, 8GB RAM (upgrading soon), no os, and 2 Xeon 5160s. 
4. HP Generic Desktop with an i3, 16 GB RAM, 4x500GB HDDs, with plans for FreeNAS
5. Cisco Catalyst 2960
6. Cisco 871
7. Cisco 1242AG Lightweight/Autonomous AP
8. Generic Patch Panel
9. 24 port Netgear web managed switch that I will likely never actually use. 
10. 8 port Netgear web managed firewall that I will likely never actually use. 

Ultimately, my plan is to have the two DL380s both at max spec (2TB Raid 10 (4TB total with 8 500GB drives), 32GB RAM, and Xeon 5492s) and each running ProxMox VE. These will host most of the VMs. Other than the Hyper-V box, which I need for one of my jobs so I have a test bed as I work remote for that job, I'm planning on having an entirely FLOSS environment. I don't think I've done a post about FLOSS yet, but if I haven't I will soon. Also, I want to upgrade the EliteDesk to have max HDD space (I'm not sure what the max the mobo supports is, but I think it's close on 16 TB), and get the FreeNAS maxed out as well. 

## The Logical Structure
I have big plans for this environment, both from a hardware perspective, and from a logical one. The Hardware, as you've seen above, is robust enough to handle most things, especially after the upgrades I'm planning. From a logical perspective though, I want four main things from this environment: 

1. A General Use Network

This will run a VDI for my kids to have machines they can play with that I can control and manage, a Media server, a file store, and a VOIP controller so I can have land-lines. 

2. A Hacking Lab

This will run Vulnerable VMs, a persistent CTF, and a few other things that I want to play with. I can also use this to build out lab images for teaching (which I do intend to do again someday...)

3. A Test Environment
One of my jobs is a remote R&D job. I need to have an environment I can mess with and COMPLETELY blow away at a whimsy, and I need to make sure it's secure and up to standards. That's going to be this. 

4. A Malware Analytics Environment
This one is going to be the last one I set up, and the one I'm most careful with. I might actually just get another hardware box and completely isolate it without having it virtualized in the lab. I'll be thinking about this in the coming weeks. 

Each of these environments are going to get their own posts with build out of software and plans on how to make it work. 

## The Goals
Other than services I absolutely need to rely on an outside source (SIP Trunk, for instance) for, I'm hoping to build this environment out using only FLOSS, and get it as productive as a corporate environment. The next goal, other than planning and starting the build out of the environment, is to figure out if I want to publically host anything on this environment, or if I just want it to be private. But that's going to be a whole other decision that I'll deal with when I have to. For the moment: Onward to the planning!
